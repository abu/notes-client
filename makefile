
.PHONY: all appstore build clean lint install build-dir

PROJECT=notes-client
BUILD_DIR=build
DATE=$(shell date +"%Y%m%d%H%M%S")
SNAPSHOT=$(BUILD_DIR)/$(PROJECT)-$(DATE)

dev:
	npm run lint

all: dev snapshot

snapshot: build-dir
	git archive --format=tar.gz -o $(SNAPSHOT).tar.gz --prefix=$(PROJECT)/ master

install:
	npm install

clean:
	rm -rf $(BUILD_DIR)

build-dir:
	mkdir -p $(BUILD_DIR)
