
'use strict';

import config from '/config/config.js';
import getAuthorization from '/js/auth.js';
import { setCookie } from '/js/cookies.js';

const oauth2 = {
  redirectURI: `${window.location.origin}/callback.html`,
  clientId: 'ek91V6IXpHx3k9F79aG28vz6ZU1Hheq2aBaGJxp2qxc3svlNdBEVlKWST2CWkZbu',
  secret: 'HsYo4S9CEA8EDcvgLkw9XoE7y4KCUW2teTg79TFS4FGGwOFvwSRULHYeQumwikjj',
};

function authorize() {
  window.location = `${config.srvURL}/index.php/apps/oauth2/authorize` +
     `?response_type=code&client_id=${oauth2.clientId}&redirect_uri=${oauth2.redirectURI}`;
}

async function tokenRequest(body, apply) {
  fetch(`${config.srvURL}/index.php/apps/oauth2/api/v1/token`, {
    method: 'POST',
    body: body,
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': 'Basic ' + btoa(`${oauth2.clientId}:${oauth2.secret}`),
    },
  })
    .then(res => res.json())
    .then((res) => {
      let lifetime = 1000 * res.expires_in; // 3600 seconds
      setCookie('token', res.access_token, lifetime);
      setCookie('tokenType', res.token_type, lifetime);
      setCookie('refreshToken', res.refresh_token, lifetime);
      apply(res);
    })
}

function getToken(code) {
  tokenRequest(
    `grant_type=authorization_code&code=${code}&redirect_uri=${oauth2.redirectURI}`,
    function () {
      window.location = window.location.origin;
    });
}

function refreshToken() {
  tokenRequest(
    `grant_type=refresh_token&refresh_token=${config.auth.refreshToken}`,
    function (res) {
      config.auth = {
        token: res.access_token,
        tokenType: res.token_type,
        refreshToken: res.refresh_token,
      };
      config.headers = {
        'Authorization': getAuthorization(),
        'Content-Type': 'application/json',
      };
    });
}

export { oauth2, authorize, getToken, refreshToken };
