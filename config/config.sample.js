
'use strict';

let config = {
  srvURL: 'https://your.owncloud.here',
  oauth2: false;
  username: 'your_login',
  password: 'your_passwd',
  timestamp: true,
  sorting: 0,
  tagging: false,
};

export default config;
