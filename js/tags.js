
'use strict';

import config from '/config/config.js';
import getAuthorization from '/js/auth.js';

export {
  assignTag,
  unassignTag,
  createTag,
  deleteTag,
  getTags,
  getTaggedFiles,
  insertDummyTag,
};

const systemtags = '/remote.php/dav/systemtags';
const systemtagsrf = '/remote.php/dav/systemtags-relations/files';

function createTreeWalker(xmlString, filter) {
  const parser = new DOMParser();
  const xmlDoc = parser.parseFromString(xmlString, 'application/xml');
  return xmlDoc.createTreeWalker(
    xmlDoc.documentElement, NodeFilter.SHOW_ELEMENT, {
      acceptNode: filter,
    }, false);
}

function filterTags(node) {
  switch (node.tagName) {
    case 'oc:display-name':
    case 'oc:user-visible':
    case 'oc:user-assignable':
    case 'oc:user-editable':
    case 'oc:id':
      return NodeFilter.FILTER_ACCEPT;
    default:
      return NodeFilter.FILTER_SKIP;
  }
}

function filterFileId(node) {
  return node.tagName == 'oc:fileid' ? NodeFilter.FILTER_ACCEPT : NodeFilter.FILTER_SKIP;
}

function collectTags(treeWalker) {
  var taglist = [];
  var tag = {};
  var value;

  while (treeWalker.nextNode()) {
    if (!(value = treeWalker.currentNode.innerHTML)) continue;
    switch (treeWalker.currentNode.tagName) {
      case 'oc:display-name':
        tag = {};
        tag.name = value;
        break;
      case 'oc:user-visible':
        tag.visible = value;
        break;
      case 'oc:user-assignable':
        tag.assignable = value;
        break;
      case 'oc:user-editable':
        tag.editable = value;
        break;
      case 'oc:id':
        tag.id = value;
        if (tag.visible && tag.assignable) {
          taglist.push(tag);
        }
        break;
      default:
        break;
    }
  }
  return taglist;
}

function collectFileId(treeWalker) {
  var filelist = [];

  while (treeWalker.nextNode()) {
    filelist.push(Number(treeWalker.currentNode.innerHTML));
  }

  return filelist;
}

function insertDummyTag(taglist) {
  taglist.splice(0, 0, {
    'name': '<all notes>',
    'id': -1,
    'visible': true,
    'assignable': false,
    'editable': false,
  });
}

async function getTags(fileId = -1) {
  var path;

  if (fileId == -1) {
    // request all tags
    path = systemtags;
  } else {
    // request all tags for file
    path = `${systemtagsrf}/${fileId}`;
  }

  const params =
		'<?xml version="1.0" encoding="utf-8" ?>' +
		'<a:propfind xmlns:a="DAV:" xmlns:oc="http://owncloud.org/ns">' +
		'<a:prop><oc:display-name/><oc:user-visible/><oc:user-assignable/>' +
    '<oc:user-editable/><oc:id/></a:prop>' +
		'</a:propfind>';

  const request = new Request(`${config.srvURL}${path}`, {
    method: 'PROPFIND',
    headers: {
      'Authorization': getAuthorization(),
      'Content-Type': 'application/xml',
    },
    body: params,
  });

  var taglist = [];
  let response = await fetch(request);

  if (response.ok) {
    let xmlString = await response.text();
    taglist = collectTags(new createTreeWalker(xmlString, filterTags));
  } else {
    console.error(`HTTP status: ${response.status}`);
  }

  if (fileId !== -1) {
    insertDummyTag(taglist)
  }

  return taglist;
}

async function createTag(name, fileId) {
  var url;

  if (fileId == -1) {
    // No file, just create the tag
    url = `${config.srvURL}${systemtags}`;
  } else {
    // Create and attach
    url = `${config.srvURL}${systemtagsrf}/${fileId}`;
  }

  const request = new Request(url, {
    method: 'POST',
    headers: config.headers,
    body: `{"name": "${name}","userVisible": true,"userAssignable": true, "userEditable": true }`,
  });

  let response = await fetch(request);
  if (response.ok) {
    await response.text();
  } else {
    console.error(`HTTP status: ${response.status}`);
  }

  return await getTags(); // systemtags
}

async function deleteTag(tagId) {
  var url = `${config.srvURL}${systemtags}/${tagId}`;
  const request = new Request(url, {
    method: 'DELETE',
    headers: config.headers,
  });

  let response = await fetch(request);
  if (response.ok) {
    await response.text();
  } else {
    console.error(`HTTP status: ${response.status}`);
  }

  return await getTags(); // systemtags
}

async function assignTag(fileId, tagId) {
  var url = `${config.srvURL}${systemtagsrf}/${fileId}/${tagId}`;

  const request = new Request(url, {
    method: 'PUT',
    headers: config.headers,
  });

  let response = await fetch(request);
  if (response.ok) {
    await response.text();
  } else {
    console.error(`HTTP status: ${response.status}`);
  }

  return await getTags(fileId);
}

async function unassignTag(fileId, tagId) {
  var url = `${config.srvURL}${systemtagsrf}/${fileId}/${tagId}`;

  const request = new Request(url, {
    method: 'DELETE',
    headers: config.headers,
  });

  let response = await fetch(request);
  if (response.ok) {
    await response.text();
  } else {
    console.error(`HTTP status: ${response.status}`);
  }

  return await getTags(fileId);
}

async function getTaggedFiles(tagId) {
  const param =
		'<oc:filter-files xmlns:d="DAV:" xmlns:oc="http://owncloud.org/ns">' +
		'<d:prop><oc:fileid /></d:prop>' +
		`<oc:filter-rules><oc:systemtag>${tagId}</oc:systemtag></oc:filter-rules>` +
		'</oc:filter-files>';

  const request = new Request(`${config.srvURL}/remote.php/webdav`, {
    method: 'REPORT',
    headers: config.headers,
    body: param,
  });

  var filelist = [];
  let response = await fetch(request);
  if (response.ok) {
    let xmlString = await response.text();
    filelist = collectFileId(new createTreeWalker(xmlString, filterFileId));
  } else {
    console.error(`HTTP status: ${response.status}`);
  }
  return filelist;
}
