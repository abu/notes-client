# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased

### Fixed

- Disable tagging must refresh the notes list

### Added

- Support authentication with OAuth2

## [v1.3.0](https://codeberg.org/abu/notes-client/releases/tag/v1.3.0) - 2021-04-23

### Added

- Change sorting from UI
- Make tagging support optional

### Fixed

- Fixed searching notes by tag

## [v1.2.0](https://codeberg.org/abu/notes-client/releases/tag/v1.2.0) - 2021-03-31

### Added

- Final tag-Support
	- Search notes by tag
	- Create & delete tags
	- Assign & unassign tags

## [v1.1.0](https://codeberg.org/abu/notes-client/releases/tag/v1.1.0) - 2021-03-15

### Added

- Tag-Support (readonly)
	- Display associated tags
	- Filter notes by tag

---

## [v1.0.0](https://codeberg.org/abu/notes-client/releases/tag/v1.0.0) - 2021-03-14

### Changed

- Using vue.js

---
