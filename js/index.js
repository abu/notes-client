
'use strict';

import config from '/config/config.js';
import getAuthorization from '/js/auth.js';
import { authorize, refreshToken } from '/js/oauth2.js';
import { getCookie } from '/js/cookies.js';
import {
  getTimeStamp,
  getSelectedNoteId,
  Note,
  InitPost,
} from './helper.js';
import {
  assignTag,
  unassignTag,
  createTag,
  deleteTag,
  getTags,
  getTaggedFiles,
  insertDummyTag,
} from './tags.js';

const api = '/index.php/apps/notes/api/v0.2/notes';

new Vue({
  el: '#app-content',
  data: {
    tagging: config.tagging,
    notelist: [],
    taglist: [],
    tagname: '',
    systaglist: [],
    deleteMode: true,
    ready: false,

    content: '',
    favorite: false,

    unauthorized: true, // Used to show/hide the auth-button
  },
  mounted() {
    this.initAuth();

    config.url = config.srvURL + api;
    config.headers = {
      'Authorization': getAuthorization(),
      'Content-Type': 'application/json',
    };

    // console.log(config.headers);
    // {
    //   "Authorization": "undefined undefined",
    //   "Content-Type": "application/json"
    // }

    document.getElementById('sorting').selectedIndex = config.sorting;
    this.loadNotes(); // all notes
    getTags().then((taglist) => {
      this.systaglist = taglist;
      this.ready = true;
    }); // all tags
  },

  computed: {
    showWhenReady() {
      return this.ready ? 'visibility: visible;' : 'visibility: hidden;'
    },
  },

  methods: {

    initAuth() {
      config.auth = {
        token: getCookie('token'),
        tokenType: getCookie('tokenType'),
        refreshToken: getCookie('refreshToken'),
      };

      console.log(config.auth);

      // {
      //   "token": "undefined",
      //   "tokenType": "undefined",
      //   "refreshToken": "undefined"
      // }

      // If authorized, start continous refreshing the tokens
      if (!(this.unauthorized = config.oauth2 && config.auth.token == '')) {
        window.setInterval(refreshToken, 300000);
      }
      console.log(this.unauthorized);
    },

    startAuth() {
      authorize();
    },

    resetTaglist() {
      this.taglist = [];
      insertDummyTag(this.taglist);
    },

    changeTagging() {
      var noteId;

      if (this.tagging && (noteId = getSelectedNoteId()) !== -1) {
        this.getAssociatedTags(noteId);
      } else {
        this.resetTaglist();
        this.loadNotes();
      }
    },

    async loadNotes(tagId = -1) {
      this.deselectNote();

      let notelist = [];
      let response = await fetch(`${config.url}?exclude=content`, {
        method: 'GET',
        headers: config.headers,
      });

      if (response.ok) {
        notelist = await response.json();
        this.sortNotes(notelist);
      } else {
        console.error('Error:', response.status);
      }

      if (tagId !== -1) {
        // Click on taglist.
        // Reduce list to notes tagged by tagId
        getTaggedFiles(tagId).then((taggedFiles) => {
          function isTagged(note) {
            return taggedFiles.includes(note.id);
          }
          this.notelist = notelist.filter(isTagged);
        });
      } else {
        this.notelist = notelist;
        this.resetTaglist();
      }
      this.deleteMode = true;
    },

    deselectNote() {
      document.getElementById('notes').selectedIndex = -1;
      this.content = '';
      this.favorite = false;
    },

    getAssociatedTags(noteId) {
      getTags(noteId).then((taglist) => {
        this.taglist = taglist;
        document.getElementById('tags').selectedIndex = -1;
      });
    },

    async selectNote(noteId) {
      // Note
      let response = await fetch(`${config.url}/${noteId}`, {
        method: 'GET',
        headers: config.headers,
      });

      let note = {};
      if (response.ok) {
        note = await response.json();
        this.favorite = note.favorite;
        this.content = note.content;
        document.getElementById('content').focus();
      } else {
        console.error('Error:', response.status);
      }

      if (this.tagging) {
        this.getAssociatedTags(noteId);
      }
      this.deleteMode = false;
    },

    deleteNote() {
      let idx;
      const sel = [];
      const notes = document.getElementById('notes');
      const { options } = notes;

      // Collect and remove selected notes from list

      while ((idx = notes.selectedIndex) !== -1) {
        sel.push(options[idx].value);
        notes.remove(idx);
      }

      // Remove collected notes from server

      sel.forEach((id) => {
        fetch(`${config.url}/${id}`, {
          method: 'DELETE',
          headers: config.headers,
        })
          .then(response => response.json())
          .then(() => {})
          .catch((error) => {
            console.error('Error:', error);
          });
      });

      this.clearNote();
    },

    clearNote(timestamp = false) {
      document.getElementById('notes').selectedIndex = -1;
      this.content = timestamp ? getTimeStamp() : '';
      this.favorite = false;
      document.getElementById('content').focus();
    },

    newNote() {
      this.resetTaglist();
      this.clearNote(config.timestamp);
    },

    doRequest(request) {
      fetch(request)
        .then(response => response.json())
        .then(() => {
          this.loadNotes();
          this.clearNote();
        })
        .catch((error) => {
          console.error('Error:', error);
        });
    },

    allow2save() {
      const { options } = document.getElementById('notes');
      let count = 0;

      for (const opt of options) {
        if (opt.selected) {
          count++;
        }
      }

      if (count > 1) {
        alert('ERROR: Too many selections');
        return false;
      }

      if (this.content === '') {
        alert('ERROR: No content');
        return false;
      }

      return true;
    },

    saveNote() {
      if (this.allow2save()) {
        const note = new Note(this.content, this.favorite);
        const id = getSelectedNoteId();
        let request;

        if (id !== -1) {
          // Update existing note

          note.id = id;
          request = new Request(`${config.url}/${id}`,
            new InitPost('PUT', config.headers, note));
        } else {
          // Create new note

          request = new Request(config.url,
            new InitPost('POST', config.headers, note));
        }
        this.doRequest(request);
      }
    },

    changeSorting() {
      config.sorting = document.getElementById('sorting').selectedIndex;
      this.sortNotes(this.notelist);
    },

    sortNotes(notelist) {
      function sortByMTime(notelist) {
        notelist.sort((lhs, rhs) => rhs.modified - lhs.modified);
      }

      function sortByFav(notelist) {
        notelist.sort((lhs, rhs) => rhs.favorite - lhs.favorite);
      }

      function sortByTitle(notelist) {
        notelist.sort((lhs, rhs) => {
          if (rhs.title > lhs.title) return -1;
          if (rhs.title < lhs.title) return 1;
          return 0;
        });
      }

      switch (config.sorting) {
        case 0: sortByMTime(notelist); break;
        case 1:
        default: sortByTitle(notelist); break;
        case 2: sortByFav(notelist); break;
      }
    },

    getKnownTag() {
      return this.systaglist.find(tag => tag.name === this.tagname);
    },

    tagNote() {
      var noteId;
      if (this.tagname && (noteId = getSelectedNoteId()) !== -1) {
        var knownTag = this.getKnownTag();

        if (knownTag) {
          // assign
          assignTag(noteId, knownTag.id).then(taglist => this.taglist = taglist);
        } else {
          // unknown/new tag
          createTag(this.tagname, getSelectedNoteId())
            .then((taglist) => {
              this.systaglist = taglist;
              this.getAssociatedTags(noteId);
            });
        }
      } // no tag
    },

    untagNote() {
      var noteId = getSelectedNoteId();
      var knownTag = this.getKnownTag();

      if (knownTag) {
        if (noteId !== -1) {
          unassignTag(noteId, knownTag.id).then(taglist => this.taglist = taglist);
        } else {
          deleteTag(knownTag.id).then((taglist) => {
            this.systaglist = taglist;
            this.resetTaglist();
          });
        }
      }
    },

    search() {
      var knownTag = this.getKnownTag();

      if (knownTag) {
        // Remove untagged notes from current list
        getTaggedFiles(knownTag.id).then((taggedFiles) => {
          function isTagged(note) {
            return taggedFiles.includes(note.id);
          }
          this.notelist = this.notelist.filter(isTagged);
        });
      }
    },

  }, // methods
}); // Vue
