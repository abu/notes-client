
'use strict';

import config from '/config/config.js';
export default getAuthorization;

function getAuthorization() {
  return config.oauth2
    ? `${config.auth.tokenType} ${config.auth.token}`
    : `Basic ${btoa(`${config.username}:${config.password}`)}`;
}
