
'use strict';

import { getToken } from '/js/oauth2.js';

(function () {
  let code = new URLSearchParams(window.location.search).get('code');
  getToken(code);
}())
